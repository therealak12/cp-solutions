#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 1005;
int n, m;
char table[MAXN][MAXN];
bool vis[MAXN][MAXN];

void dfs(int i, int j)
{
    vis[i][j] = true;
    for (int k = -1; k <= 1; k++)
    {
        if (k == 0)
            continue;
        if (i + k < n && i + k >= 0 && table[i + k][j] == 'b' && !vis[i + k][j])
        {
            dfs(i + k, j);
        }
        if (j + k < m && j + k >= 0 && table[i][j + k] == 'b' && !vis[i][j + k])
        {
            dfs(i, j + k);
        }
    }
}

int main()
{
    FASTER;
    cin >> n >> m;
    bool b_exists;
    bool w_exists;
    bool possible = true;
    bool need_s_in_row = false;
    bool need_s_in_col = false;
    for (int i = 0; i < n; i++)
    {
        string next_line;
        cin >> next_line;
        b_exists = false;
        w_exists = false;
        for (int j = 0; j < m; j++)
        {
            if (next_line[j] == '#')
            {
                if (b_exists && w_exists)
                {
                    possible = false;
                }
                table[i][j] = 'b';
                b_exists = true;
                w_exists = false;
            }
            else
            {
                table[i][j] = 'w';
                w_exists = true;
            }
        }
        if (!b_exists)
        {
            need_s_in_row = true;
        }
    }
    for (int j = 0; j < m; j++)
    {
        b_exists = false;
        w_exists = false;
        for (int i = 0; i < n; i++)
        {
            if (table[i][j] == 'b')
            {
                if (b_exists && w_exists)
                {
                    possible = false;
                }
                b_exists = true;
                w_exists = false;
            }
            else
            {
                w_exists = true;
            }
        }
        if (!b_exists)
        {
            need_s_in_col = true;
        }
    }

    possible &= !(need_s_in_col ^ need_s_in_row);
    if (!possible)
    {
        cout << "-1\n";
        return 0;
    }

    ll ans = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (table[i][j] == 'b' && !vis[i][j])
            {
                dfs(i, j);
                ans += 1;
            }
        }
    }
    cout << ans << "\n";

    return 0;
}