#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n;

int find_lps_length(string pat)
{
    int pat_l = pat.size();
    int lps[pat_l];
    lps[0] = 0;
    int i = 1;
    int len = 0;
    while (i < pat_l)
    {
        if (pat[i] == pat[len])
        {
            len += 1;
            lps[i] = len;
            i += 1;
        }
        else
        {
            if (len != 0)
            {
                len = lps[len - 1];
            }
            else
            {
                lps[i] = 0;
                i += 1;
            }
        }
    }
    return lps[pat_l - 1];
}

int main()
{
    FASTER;
    cin >> n;
    string next;
    cin >> next;
    string ans = next;
    string c;
    for (int i = 1; i < n; i++)
    {
        cin >> next;
        int ans_l = ans.size();
        int next_l = next.size();
        if (ans_l > next_l)
        {
            c = next + "#" + ans.substr(ans_l - next_l);
        }
        else
        {
            c = next.substr(0, ans_l) + "#" + ans;
        }
        int max_l = min(find_lps_length(c), min(ans_l, next_l));
        if (max_l < next_l)
        {
            ans.append(next.substr(max_l));
        }
    }
    cout << ans << "\n";

    return 0;
}