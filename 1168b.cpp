#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER                    \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0)

int main()
{
    FASTER;
    string s;
    cin >> s;
    int n = s.size();

    ll ans = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = i; j < n; j++)
        {
            for (int x = i; x < j; x++)
            {
                for (int k = 1; (x + 2 * k) <= j; k++)
                {
                    if (s[x] == s[x + k] && s[x] == s[x + 2 * k])
                    {
                        ans += (n - j) * (x - i + 1);
                        i = x;
                        j = n;
                        k = n;
                        x = j + 1;
                        break;
                    }
                }
            }
        }
    }

    cout << ans << endl;
    return 0;
}