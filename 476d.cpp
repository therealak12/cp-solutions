#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

struct AnsLine
{
    int line[4];
};

int n, k;
vector<AnsLine> ans;

int main()
{
    FASTER;
    cin >> n >> k;
    int m = 1;
    for (int i = 0; i < n; i++)
    {
        ans.push_back({{m, m + 1, m + 2, m + 4}});
        m += 6;
    }
    cout << k * (m - 2) << "\n";
    for (int i = 0; i < n; i++)
    {
        cout << k * ans[i].line[0] << " " << k * ans[i].line[1] << " " << k * ans[i].line[2] << " " << k * ans[i].line[3] << "\n";
    }

    // There are only three odd numbers in each 6 consecutive numbers.

    return 0;
}