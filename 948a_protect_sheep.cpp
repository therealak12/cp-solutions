// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>
#include <stack>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXR = 505;
int r, c;
char table[MAXR][MAXR];

bool has_wolf_neighbour(int i, int j)
{
    for (int k = -1; k <= 1; k++)
    {
        if (k == 0)
            continue;
        if (i + k >= 0 && i + k < r && table[i + k][j] == 'W')
        {
            return true;
        }
        if (j + k >= 0 && j + k < c && table[i][j + k] == 'W')
        {
            return true;
        }
    }
    return false;
}

int main()
{
    FASTER;
    cin >> r >> c;
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            cin >> table[i][j];
        }
    }
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            if (table[i][j] == 'S' && has_wolf_neighbour(i, j))
            {
                cout << "NO\n";
                return 0;
            }
            if (table[i][j] == '.')
            {
                table[i][j] = 'D';
            }
        }
    }
    cout << "YES\n";
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            cout << table[i][j];
        }
        cout << "\n";
    }

    return 0;
}