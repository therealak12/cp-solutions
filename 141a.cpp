#include <iostream>
using namespace std;

int i, j, k;
int max_size = 100;
int main()
{
    ios_base::sync_with_stdio(false);

    char guest[max_size];
    char host[max_size];
    char mixed[max_size];

    cin >> guest;
    cin >> host;
    cin >> mixed;

    int occur_count[26] = {0};
    int available_count[26] = {0};

    for (i = 0; i < max_size && guest[i] != 0; ++i)
    {
        char c = guest[i];
        ++occur_count[c - 65];
    }

    for (i = 0; i < max_size && host[i] != 0; ++i)
    {
        char c = host[i];
        ++occur_count[c - 65];
    }

    for (i = 0; i < max_size && mixed[i] != 0; ++i)
    {
        char c = mixed[i];
        ++available_count[c - 65];
    }

    for (i = 0; i < 26; ++i)
    {
        if (available_count[i] != occur_count[i])
        {
            cout << "NO" << endl;
            break;
        }
    }
    if (i == 26)
    {
        cout << "YES" << endl;
    }

    return 0;
}