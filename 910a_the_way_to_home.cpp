// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>
#include <stack>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 101;
int n, d;
string nums;

int main()
{
    FASTER;
    cin >> n >> d;
    cin >> nums;
    if (nums[0] == '0')
    {
        cout << "-1\n";
        return 0;
    }
    int current_index = 0;
    int next_index = 0;
    int next_index_guess = 0;
    int ans = 0;
    while (true)
    {
        if (current_index == n - 1) {
            cout << ans << "\n";
            return 0;
        }
        next_index_guess = current_index;
        while (true)
        {
            next_index_guess += 1;
            if (next_index_guess >= n || next_index_guess - current_index > d) {
                break;
            }
            if (nums[next_index_guess] == '1')
            {
                next_index = next_index_guess;
            }
        }
        if (next_index == current_index)
        {
            cout << "-1\n";
            return 0;
        }

        ans += 1;
        current_index = next_index;
    }

    return 0;
}