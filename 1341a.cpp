#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int t;

int main()
{
    FASTER;
    cin >> t;
    for (int i = 0; i < t; i++)
    {
        int n, a, b, c, d;
        cin >> n >> a >> b >> c >> d;
        a = a - b;
        b = a + 2 * b;
        c = c - d;
        d = c + 2 * d;

        bool yes = false;
        for (int j = a * n; j <= n * b; j += 1)
        {
            if (j >= c && j <= d)
            {
                cout << "YES\n";
                yes = true;
                break;
            } else if (j > d) {
                break;
            }
        }
        if (!yes)
        {
            cout << "NO\n";
        }
    }

    return 0;
}