#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define pii pair<int, int>

string a, b;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin >> a;
    cin >> b;
    int n = a.size();
    int seven_needs, four_needs;
    seven_needs = four_needs = 0;
    for (int i = 0; i < n; i++)
    {
        if (a[i] == b[i])
        {
            continue;
        }
        if (a[i] == '4')
        {
            seven_needs += 1;
        }
        else
        {
            four_needs += 1;
        }
    }

    cout << max(four_needs, seven_needs) << endl;

    return 0;
}