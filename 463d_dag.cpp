#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER                    \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0)

const int MAX_N = 5005;
const int MAX_K = 6;

int indexes[MAX_K][MAX_N];
int numbers[MAX_K][MAX_N];
int graph[MAX_N][MAX_N];
int n, k;
stack<int> topol_stack;

void topol_sort(int index, bool visited[])
{
    visited[index] = true;
    for (int i = 0; i < n; i++)
    {
        if (graph[index][i] == 1 && !visited[i])
        {
            topol_sort(i, visited);
        }
    }
    topol_stack.push(index);
}

int main()
{
    FASTER;
    cin >> n >> k;
    for (int i = 0; i < k; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cin >> numbers[i][j];
            numbers[i][j] -= 1;
            indexes[i][numbers[i][j]] = j;
        }
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            int arr_index = 1;
            while (arr_index < k && indexes[arr_index][numbers[0][j]] < indexes[arr_index][numbers[0][i]])
            {
                arr_index += 1;
            }
            if (arr_index == k)
            {
                graph[j][i] = 1;
            }
        }
    }

    bool visited[n];
    for (int i = 0; i < n; i++)
    {
        visited[i] = false;
    }

    for (int i = 0; i < n; i++)
    {
        if (!visited[i])
        {
            topol_sort(i, visited);
        }
    }

    int dist[n];
    for (int i = 0; i < n; i++)
    {
        dist[i] = INT32_MIN;
    }
    dist[0] = 0;
    while (!topol_stack.empty())
    {
        int u = topol_stack.top();
        topol_stack.pop();
        if (dist[u] == INT32_MIN)
        {
            dist[u] = 0;
        }
        for (int i = 0; i < n; i++)
        {
            if (graph[u][i] == 1 && dist[i] < dist[u] + 1)
            {
                dist[i] = dist[u] + 1;
            }
        }
    }

    int ans = 0;
    for (int i = 0; i < n; i++)
    {
        ans = max(ans, dist[i] + 1);
    }
    cout << ans << endl;

    return 0;
}