// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>
#include <stack>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 1000005;
int n, k;
string s;
vi one_counts(MAXN, 0);

int solve(string ti, int ti_ones)
{
    int tsize = ti.size();
    vi dp(tsize, 0);
    dp[0] = (ti[0] != '1');
    int one_count = (ti[0] == '1');
    int ans = ti_ones;
    for (int i = 1; i < tsize; i++)
    {
        dp[i] = (ti[i] != '1');
        dp[i] += min(one_count, dp[i - 1]);
        one_count += (ti[i] == '1');
        ans = min(ans, dp[i] + ti_ones - one_count);
    }
    return ans;
}

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n >> k;
        cin >> s;
        int ones = 0;
        vi one_counts(k, 0);
        vector<string> tis(k);
        for (int i = 0; i < n; i++)
        {
            if (s[i] == '1')
            {
                ones += 1;
                one_counts[i % k] += 1;
            }
            tis[i % k] += s[i];
        }
        int ans = max(ones - 1, 0);
        for (int i = 0; i < k; i++)
        {
            ans = min(ans, solve(tis[i], one_counts[i]) + (ones - one_counts[i]));
        }
        cout << ans << "\n";
    }

    return 0;
}