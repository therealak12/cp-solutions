#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER                    \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0)

const int MAXN = 100005;
map<int, int> luckyRepCount;
const int MAXL = 1022;
int fact[MAXN];
int inv[MAXN];

bool isLucky(int number)
{
    while (number != 0)
    {
        if (number % 10 != 4 && number % 10 != 7)
        {
            return false;
        }
        number /= 10;
    }
    return true;
}

void init()
{
    fact[0] = 1;
    for (int i = 1; i < MAXN; i++)
    {
        fact[i] = (ll)fact[i - 1] * i % MOD;
    }
    inv[1] = 1;
    for (int i = 2; i < MAXN; i++)
    {
        inv[i] = MOD - (ll)(MOD / i) * inv[MOD % i] % MOD;
    }
    inv[0] = 1;
    for (int i = 1; i < MAXN; i++)
    {
        inv[i] = (ll)inv[i - 1] * inv[i] % MOD;
    }
}

int combination(int n, int k)
{
    if (n < 0 || k < 0 || n < k)
    {
        return 0;
    }

    return (ll)fact[n] * inv[n - k] % MOD * inv[k] % MOD;
}

int main()
{
    FASTER;

    init();
    int n, k;
    cin >> n >> k;
    int distinctLuckyCount = 0;
    int unluckyCount = 0;
    vector<int> luckyNumbers;
    for (int i = 0; i < n; i++)
    {
        int currentNum;
        cin >> currentNum;
        if (isLucky(currentNum))
        {
            if (luckyRepCount.find(currentNum) == luckyRepCount.end())
            {
                luckyRepCount[currentNum] = 1;
                distinctLuckyCount += 1;
                luckyNumbers.push_back(currentNum);
            }
            else
            {
                luckyRepCount[currentNum] += 1;
            }
        }
        else
        {
            unluckyCount += 1;
        }
    }

    int dp[2][distinctLuckyCount + 1];
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j <= distinctLuckyCount; j++)
        {
            dp[i][j] = 0;
        }
    }

    for (int i = 0; i < 2; i++)
    {
        dp[i][0] = 1;
    }

    if (distinctLuckyCount)
    {
        for (int i = 1; i <= distinctLuckyCount + 1; i++)
        {
            int turn = i % 2;
            for (int j = 1; j <= distinctLuckyCount; j++)
            {
                dp[turn][j] = (dp[1 - turn][j] + (ll)luckyRepCount[luckyNumbers[i - 1]] * dp[1 - turn][j - 1] % MOD) % MOD;
            }
        }
    }

    int ans = 0;
    for (int i = 0; i <= distinctLuckyCount; i++)
    {
        ans = ((ll)ans + (ll)combination(unluckyCount, k - i) * dp[luckyRepCount[distinctLuckyCount % 2]][i] % MOD) % MOD;
    }
    cout << ans << endl;
    return 0;
}