#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER                    \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0)

int n, m;
vi numbers;
vi dp;

int main()
{
    FASTER;
    cin >> n >> m;
    dp.resize(n);
    dp.assign(n, 0);

    numbers.resize(n);
    for (int i = 0; i < n; i++)
    {
        cin >> numbers[i];
        numbers[i] -= 1;
        double redundant;
        cin >> redundant;
    }

    for (int i = 0; i < n; i++)
    {
        int type = numbers[i];
        for (int j = type; j >= 0; j--)
        {
            dp[type] = max(dp[type], 1 + dp[j]);
        }
    }

    int ans = 0;
    for (int i = 0; i < n; i++)
    {
        ans = max(ans, dp[i]);
    }
    cout << n - ans << endl;

    return 0;
}