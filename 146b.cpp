#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER                    \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0)

int a, b;
int get_mask(int number)
{
    int mask = 0;
    int order = 0;
    while (number != 0)
    {
        int digit = number % 10;
        if (digit == 4 || digit == 7)
        {
            mask += pow(10, order) * digit;
            order += 1;
        }
        number /= 10;
    }
    return mask;
}

int main()
{
    FASTER;
    cin >> a >> b;
    b = get_mask(b);
    a = a + 1;
    while (get_mask(a) != b)
    {
        a += 1;
    }
    cout << a << endl;
    return 0;
}