#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n, sum;
vi coins;
vector<vector<vector<bool>>> dp;

void resize_arrays()
{
    coins.resize(n);
    dp.resize(n + 1);
    for (int i = 0; i <= n; i++)
    {
        dp[i].resize(sum + 1);
        for (int j = 0; j <= sum; j++)
        {
            dp[i][j].resize(sum + 1);
        }
    }
}

void init_dp()
{
    for (int i = 0; i <= n; i++)
    {
        dp[i][0][0] = true;
    }
}

int main()
{
    FASTER;
    cin >> n >> sum;
    resize_arrays();
    for (int i = 0; i < n; i++)
    {
        cin >> coins[i];
    }
    init_dp();

    for (int i = 1; i <= n; i++)
    {
        for (int j = 0; j <= sum; j++)
        {
            for (int k = 0; k <= j; k++)
            {
                dp[i][j][k] = dp[i - 1][j][k];
                if (j >= coins[i - 1])
                {
                    dp[i][j][k] = dp[i][j][k] || dp[i - 1][j - coins[i - 1]][k];
                    if (k >= coins[i - 1])
                    {
                        dp[i][j][k] = dp[i][j][k] || dp[i - 1][j - coins[i - 1]][k - coins[i - 1]];
                    }
                }
            }
        }
    }

    int ans = 0;
    for (int i = 0; i <= sum; i++)
    {
        if (dp[n][sum][i] == true)
        {
            ans += 1;
        }
    }
    cout << ans << "\n";
    for (int i = 0; i <= sum; i++)
    {
        if (dp[n][sum][i] == true)
        {
            cout << i << " ";
        }
    }
    cout << "\n";

    return 0;
}