#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n;
vi colors;
vvi neighbs;
vi dp;
vi bests;

void find_root_ans(int root, int parent = -1)
{
    dp[root] = colors[root];
    for (int child : neighbs[root])
    {
        if (child == parent)
        {
            continue;
        }
        find_root_ans(child, root);
        dp[root] += max(dp[child], 0);
    }
}

void find_childrent_ans(int root, int parent = -1)
{
    bests[root] = dp[root];
    for (int child : neighbs[root])
    {
        if (child == parent)
        {
            continue;
        }
        dp[root] -= max(0, dp[child]);
        dp[child] += max(dp[root], 0);
        find_childrent_ans(child, root);
        dp[child] -= max(dp[root], 0);
        dp[root] += max(0, dp[child]);
    }
}

int main()
{
    FASTER;
    cin >> n;
    colors.resize(n);
    neighbs.resize(n);
    dp.resize(n);
    bests.resize(n);
    for (int i = 0; i < n; i++)
    {
        cin >> colors[i];
        if (colors[i] == 0)
        {
            colors[i] = -1;
        }
    }

    for (int i = 0; i < n - 1; i++)
    {
        int u, v;
        cin >> u >> v;
        u -= 1;
        v -= 1;
        neighbs[u].push_back(v);
        neighbs[v].push_back(u);
    }

    find_root_ans(0);
    find_childrent_ans(0);

    for (int best : bests)
    {
        cout << best << " ";
    }
    cout << endl;

    return 0;
}