// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 100005;
int n;
int nums[MAXN];
vector<bool> checked(MAXN, false);

int find_most(int start)
{
    int most = 0;
    for (int k = 2; k * start <= n; k++)
    {
        int good_count = 0;
        if (nums[start] < nums[k * start])
        {
            good_count = 1 + find_most(k * start);
        }
        most = max(most, good_count);
    }
    return most;
}

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n;
        for (int i = 1; i <= n; i++)
        {
            cin >> nums[i];
        }
        int ans = 1;
        for (int i = 1; i <= n; i++)
        {
            ans = max(ans, 1 + find_most(i));
        }
        cout << ans << "\n";
    }

    return 0;
}