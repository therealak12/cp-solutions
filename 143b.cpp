#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)

string raw_num;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin >> raw_num;

    vector<char> final_num;
    int dash_index = raw_num.find('-');
    int dot_index = raw_num.find('.');
    if (dash_index >= 0)
    {
        final_num.push_back(')');
        cout << '(';
    }
    if (dot_index >= 0)
    {
        if (dot_index + 1 < raw_num.length())
        {
            if (dot_index + 2 < raw_num.length())
            {
                final_num.push_back(raw_num[dot_index + 2]);
            }
            else
            {
                final_num.push_back('0');
            }
            final_num.push_back(raw_num[dot_index + 1]);
        }
        else
        {
            final_num.push_back('0');
            final_num.push_back('0');
        }
    }
    else
    {
        final_num.push_back('0');
        final_num.push_back('0');
    }
    final_num.push_back('.');
    int last_int_index = dot_index >= 0 ? dot_index - 1 : raw_num.size() - 1;
    for (int i = last_int_index; i >= 0; --i)
    {
        if (raw_num[i] == '-')
        {
            continue;
        }
        if ((i - last_int_index) % 3 == 0 && i != last_int_index)
        {
            final_num.push_back(',');
        }
        final_num.push_back(raw_num[i]);
    }
    cout << '$';
    for (int i = final_num.size() - 1; i >= 0; --i)
    {
        cout << final_num[i];
    }
    cout << endl;

    return 0;
}