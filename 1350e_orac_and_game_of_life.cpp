#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 1005;
int n, m, t, i, j;
ll p;
bool table[MAXN][MAXN];
bool changeable[MAXN][MAXN];

void determine_states()
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            for (int k = -1; k <= 1; k++)
            {
                if (k == 0)
                    continue;
                if (i + k < n && i + k >= 0 && table[i][j] == table[i + k][j])
                {
                    changeable[i][j] = true;
                }
                if (j + k < m && j + k >= 0 && table[i][j] == table[i][j + k])
                {
                    changeable[i][j] = true;
                }
            }
        }
    }
}

bool vis[MAXN][MAXN];
int dist[MAXN][MAXN];

void bfs()
{

    queue<pii> q;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (changeable[i][j])
            {
                vis[i][j] = true;
                dist[i][j] = 0;
                q.push(mp(i, j));
            }
            else
            {
                vis[i][j] = false;
                dist[i][j] = INT32_MAX;
            }
        }
    }

    while (!q.empty())
    {
        int x = q.front().first;
        int y = q.front().second;
        q.pop();
        for (int i = -1; i <= 1; i++)
        {
            if (i == 0)
                continue;
            if (x + i >= 0 && x + i < n && !vis[x + i][y])
            {
                vis[x + i][y] = true;
                dist[x + i][y] = dist[x][y] + 1;
                q.push(mp(x + i, y));
            }
            if (y + i >= 0 && y + i < m && !vis[x][y + i])
            {
                vis[x][y + i] = true;
                dist[x][y + i] = dist[x][y] + 1;
                q.push(mp(x, y + i));
            }
        }
    }
}

int main()
{
    FASTER;
    cin >> n >> m >> t;
    for (int i = 0; i < n; i++)
    {
        string next_line;
        cin >> next_line;
        for (int j = 0; j < m; j++)
        {
            table[i][j] = next_line[j] - '0';
            changeable[i][j] = false;
        }
    }
    determine_states();
    bfs();
    for (int query = 0; query < t; query++)
    {
        cin >> i >> j >> p;
        i -= 1;
        j -= 1;
        if (changeable[i][j])
        {
            cout << ((p % 2) + table[i][j]) % 2 << "\n";
        }
        else
        {
            if (dist[i][j] == INT32_MAX || p <= dist[i][j])
            {
                cout << table[i][j] << "\n";
            }
            else
            {
                cout << (abs(p - dist[i][j]) % 2 + table[i][j]) % 2 << "\n";
            }
        }
    }

    return 0;
}