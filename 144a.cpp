#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)

int n;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin >> n;
    int heights[n];
    int max_h, max_id, min_h, min_id;
    max_h = -1;
    min_h = INT32_MAX;
    for (int i = 0; i < n; ++i)
    {
        cin >> heights[i];
        if (heights[i] > max_h)
        {
            max_id = i;
            max_h = heights[i];
        }
        if (heights[i] <= min_h)
        {
            min_id = i;
            min_h = heights[i];
        }
    }

    int ans = (n - min_id - 1) + (max_id);
    if (max_id > min_id)
    {
        ans -= 1;
    }
    cout << ans << endl;

    return 0;
}