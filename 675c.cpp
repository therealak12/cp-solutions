// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 1e5 + 5;
int n;
vi values(MAXN);

int main()
{
    FASTER;
    cin >> n;
    int current_index = 0;
    for (int i = 0; i < n; i++)
    {
        cin >> values[i];
        if (values[i] == 0)
        {
            current_index = i;
        }
    }
    int ans = 0;
    int current_value = values[current_index];
    for (int i = 0; i < n - 1; i++)
    {
        current_index = (current_index + 1) % n;
        if (current_value != 0)
        {
            ans += 1;
        }
        current_value += values[current_index];
    }
    cout << ans << "\n";

    return 0;
}