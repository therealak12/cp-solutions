#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 200005;
int n;
vector<vii> graph(MAXN);
vi values(MAXN, 0);
vi ans(MAXN, 0);
vector<ll> heights(MAXN, 0);
int max_h;
vvi parents(MAXN);

void init()
{
    for (int i = 0; i < n; i++)
    {
        parents[i].resize(max_h + 1);
    }
}

void dfs(int root, ll height, int parent)
{
    parents[root][0] = parent;
    heights[root] = height;
    ans[root] = 1;
    for (int i = 1; i <= max_h; i++)
    {
        parents[root][i] = parents[parents[root][i - 1]][i - 1];
    }
    for (int i = 0; i < graph[root].size(); i++)
    {
        dfs(graph[root][i].first, height + graph[root][i].second, root);
    }
}

void reduce_first_bad_parent(int start)
{
    ll thresh = heights[start] - values[start];
    for (int i = max_h; i >= 0; i--)
    {
        if (thresh <= heights[parents[start][i]])
        {
            start = parents[start][i];
        }
    }
    if (start == 0)
        return;
    ans[parents[start][0]] -= 1;
}

void solve_dfs(int start)
{
    if (start != 0)
    {
        reduce_first_bad_parent(start);
    }
    for (pii edge : graph[start])
    {
        solve_dfs(edge.first);
        ans[start] += ans[edge.first];
    }
}

int main()
{
    FASTER;
    cin >> n;
    max_h = ceil(log2(n));
    init();
    for (int i = 0; i < n; i++)
    {
        cin >> values[i];
    }
    for (int i = 0; i < n - 1; i++)
    {
        int p, w;
        cin >> p >> w;
        p -= 1;
        graph[p].push_back(mp(i + 1, w));
    }

    dfs(0, 0, 0);
    solve_dfs(0);

    for (int i = 0; i < n; i++)
    {
        cout << ans[i] - 1 << " ";
    }
    cout << "\n";

    return 0;
}
