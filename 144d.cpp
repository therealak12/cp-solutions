#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define pii pair<int, int>

int n, m, s, l;

class CustomeCompare
{
public:
    bool operator()(pii a, pii b)
    {
        return a.second > b.second;
    }
};

void dijkstra(vector<pii> adjs[], int dist[])
{
    bool in_tree[n];
    for (int i = 0; i < n; ++i)
    {
        in_tree[i] = false;
    }

    priority_queue<pii, vector<pii>, CustomeCompare> q;
    dist[s] = 0;
    q.push(mp(s, 0));
    while (!q.empty())
    {
        pii next_node = q.top();
        q.pop();
        in_tree[next_node.first] = true;
        if (dist[next_node.first] != next_node.second)
        {
            continue;
        }
        for (auto edge : adjs[next_node.first])
        {
            int weight = edge.second;
            int dest = edge.first;
            if (weight > 0 && !in_tree[dest] && dist[dest] > dist[next_node.first] + weight)
            {
                dist[dest] = dist[next_node.first] + weight;
                q.push(mp(dest, dist[dest]));
            }
        }
    }
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> n >> m >> s;
    s -= 1;
    vector<pii> adjs[n];

    int dist[n];
    for (int i = 0; i < n; ++i)
    {
        dist[i] = INT32_MAX;
    }

    int u, v, w;
    for (int i = 0; i < m; ++i)
    {
        cin >> u >> v >> w;
        u -= 1;
        v -= 1;
        adjs[u].push_back(mp(v, w));
        adjs[v].push_back(mp(u, w));
    }
    cin >> l;
    dijkstra(adjs, dist);
    int ans = 0;
    // for (int i = 0; i < n; i++)
    // {
    //     cout << dist[i] << " ";
    // }
    // cout << endl;

    for (int i = 0; i < n; ++i)
    {
        if (dist[i] == l)
        {
            ans += 1;
        }
        else if (dist[i] < l)
        {
            for (auto edge : adjs[i])
            {
                if (dist[i] + edge.second > l &&
                    (dist[edge.first] + (edge.second - (l - dist[i])) > l ||
                     (dist[edge.first] + (edge.second - (l - dist[i])) == l && i < edge.first)))
                {
                    ans += 1;
                }
            }
        }
    }

    cout << ans << endl;
    return 0;
}