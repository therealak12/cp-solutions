#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

string s;
int n;
vii palindromes;
vector<ll> right_count;

void init_arrs()
{
    right_count.resize(n);
    for (int i = 0; i < n; i++)
    {
        right_count[i] = 0;
    }
}

void find_palindromes()
{
    for (int i = n - 1; i >= 0; --i)
    {
        palindromes.push_back(mp(i, i));
        right_count[i] += 1;
        if (i != n - 1)
        {
            right_count[i] += right_count[i + 1];
        }
        int k = 1;
        while (i - k >= 0 && i + k < n && s[i - k] == s[i + k])
        {
            palindromes.push_back(mp(i - k, i + k));
            right_count[i - k] += 1;
            k += 1;
        }
        k = 0;
        while (i - k >= 0 && i + k + 1 < n && s[i - k] == s[i + k + 1])
        {
            palindromes.push_back(mp(i - k - 1, i + k + 1));
            right_count[i - k] += 1;
            k += 1;
        }
    }
}

int main()
{
    FASTER;
    cin >> s;
    n = s.size();
    init_arrs();

    find_palindromes();

    ll ans = 0;
    for (int i = 0; i < palindromes.size(); i++)
    {
        if (palindromes[i].second + 1 < n)
        {
            ans += right_count[palindromes[i].second + 1];
        }
    }
    cout << ans << "\n";

    return 0;
}