#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n;
string s;
vvi dp;

void init_dp()
{
    dp.resize(n);
    for (int i = 0; i < n; i++)
    {
        dp[i].resize(n, INT32_MAX);
        dp[i][i] = 1;
        for (int j = 0; j < i; j++)
        {
            dp[i][j] = 0;
        }
    }
}

int main()
{
    FASTER;
    cin >> n;
    cin >> s;
    init_dp();
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j + i < n; j++)
        {
            for (int k = j; k <= j + i; k++)
            {
                int tdp = 0;
                if (k >= 1)
                {
                    tdp += min(dp[j + 1][k], dp[j][k - 1]);
                }
                else
                {
                    tdp += dp[j + 1][k];
                }
                if (s[j] != s[k] || k == j)
                {
                    tdp += 1;
                }

                if (k < n - 1)
                {
                    if (k < n - 2)
                    {
                        tdp += min(dp[k + 1][j + i - 1], dp[k + 2][j + i]);
                    }
                    else
                    {
                        tdp += dp[k + 1][j + i - 1];
                    }
                    if ((k + 1 <= j + i && s[k + 1] != s[j + i]) || k + 1 == j + i)
                    {
                        tdp += 1;
                    }
                }
                dp[j][j + i] = min(dp[j][j + i], tdp);
            }
        }
    }

    cout << dp[0][n - 1] << "\n";

    return 0;
}