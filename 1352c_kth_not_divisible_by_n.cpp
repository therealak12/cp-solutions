// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        int n, k;
        cin >> n >> k;
        int steps = k / (n - 1);
        int rem = k % (n - 1);
        int ans;
        if (rem == 0)
        {
            ans = steps * n - 1;
        } else {
            ans = steps * n + rem;
        }
        cout << ans << "\n";
    }

    return 0;
}