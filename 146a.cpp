#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER                    \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0)

int n;
string number;

int main()
{
    FASTER;
    cin >> n;
    cin >> number;
    int index = n - 1;
    int sum = 0;
    while (index >= 0)
    {
        int digit = number[index] - '0';
        if (digit != 4 && digit != 7)
        {
            cout << "NO" << endl;
            return 0;
        }
        if (index >= (n / 2))
        {
            sum += digit;
        }
        else
        {
            sum -= digit;
        }
        index -= 1;
    }
    if (sum == 0)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
    return 0;
}