// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>
#include <stack>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 505;
int n, m, k;
char table[MAXN][MAXN];
bool visited[MAXN][MAXN];

void print_table()
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cout << table[i][j];
        }
        cout << '\n';
    }
}

int main()
{
    FASTER;
    cin >> n >> m >> k;
    int start_i, start_j;
    int total_empty = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cin >> table[i][j];
            visited[i][j] = false;
            if (table[i][j] == '.')
            {
                start_i = i;
                start_j = j;
                total_empty += 1;
            }
        }
    }
    if (k == 0)
    {
        print_table();
        return 0;
    }

    queue<pii> bfs_queue;
    bfs_queue.push(mp(start_i, start_j));
    visited[start_i][start_j] = true;
    int current_i, current_j;
    int count_of_empty = 0;
    while (!bfs_queue.empty())
    {
        pii current_node = bfs_queue.front();
        bfs_queue.pop();
        current_i = current_node.first;
        current_j = current_node.second;
        count_of_empty += 1;
        if (count_of_empty > total_empty - k)
        {
            table[current_i][current_j] = 'X';
        }
        for (int r = -1; r <= 1; r++)
        {
            if (r == 0)
                continue;
            if (current_i + r >= 0 && current_i + r < n && !visited[current_i + r][current_j] && table[current_i + r][current_j] == '.')
            {
                visited[current_i + r][current_j] = true;
                bfs_queue.push(mp(current_i + r, current_j));
            }
            if (current_j + r >= 0 && current_j + r < m && !visited[current_i][current_j + r] && table[current_i][current_j + r] == '.')
            {
                visited[current_i][current_j + r] = true;
                bfs_queue.push(mp(current_i, current_j + r));
            }
        }
    }

    print_table();

    return 0;
}