#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n;
string t, s;
vector<char> ans;
string three_perms[6] = {"abca", "acba", "bacb", "bcab", "cabc", "cbac"};
string six_perms[6] = {"aabbcc", "aaccbb", "bbaacc", "bbccaa", "ccaabb", "ccbbaa"};

int main()
{
    FASTER;
    cin >> n;

    ans.resize(n);
    cin >> t >> s;

    for (int i = 0; i < 6; i++)
    {
        if (three_perms[i].find(t) == string::npos && three_perms[i].find(s) == string::npos)
        {
            cout << "YES\n";
            for (int j = 0; j < n; j++)
            {
                cout << three_perms[i][0] << three_perms[i][1] << three_perms[i][2];
            }
            cout << "\n";
            return 0;
        }
    }
    for (int i = 0; i < 6; i++)
    {
        if (six_perms[i].find(t) == string::npos && six_perms[i].find(s) == string::npos)
        {

            cout << "YES\n";
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < n; k++)
                {
                    cout << three_perms[i][j];
                }
            }
            cout << "\n";
            return 0;
        }
        else if (i == 5)
        {
            cout << "NO\n";
        }
    }

    return 0;
}