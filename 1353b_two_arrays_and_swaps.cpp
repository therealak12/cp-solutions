// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>
#include <stack>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 30;
int n, k;
int a[MAXN], b[MAXN];

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n >> k;
        for (int i = 0; i < n; i++)
        {
            cin >> a[i];
        }
        for (int i = 0; i < n; i++)
        {
            cin >> b[i];
        }
        sort(b, b + n);
        sort(a, a + n);
        ll sum = 0;
        for (int i = 0; i < n; i++)
        {
            if (i < k)
            {
                sum += max(a[i], b[n - i - 1]);
            }
            else
            {
                sum += a[i];
            }
        }
        cout << sum << "\n";
    }

    return 0;
}