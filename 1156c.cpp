#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n, z;
vi nums;

int main()
{
    FASTER;
    cin >> n >> z;
    nums.resize(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nums[i];
    }
    sort(nums.begin(), nums.end());
    int greater_index = (n + 1) / 2;

    int ans = 0;
    int smaller_index = 0;
    while (greater_index < n)
    {
        while (greater_index < n && (nums[greater_index] - nums[smaller_index] < z))
        {
            greater_index += 1;
        }
        if (greater_index < n && nums[greater_index] - nums[smaller_index] >= z)
        {
            ans += 1;
            greater_index += 1;
            smaller_index += 1;
        }
    }
    cout << ans << "\n";

    return 0;
}