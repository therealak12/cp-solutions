#include <bits/stdc++.h>

using namespace std;
typedef long long ll;
#define mp(a, b) make_pair(a, b)

struct Edge
{
    int source;
    int destin;
    int weight;
    int ramp_id;
};

struct Node
{
    int coord;
    vector<Edge> adjs;
    Edge to_edge;
};

struct NodeId
{
    int coord;
    int index;
};

bool operator<(const NodeId node_id1, const NodeId node_id2)
{
    return node_id1.coord < node_id2.coord;
}

const int MAXN = 1e6;
int n, L, node_count = 0;
Node nodes[MAXN];
vector<NodeId> node_ids;
int dist[MAXN];

Edge create_edge(int source, int destin, int weight, int ramp_id)
{
    Edge edge;
    edge.source = source;
    edge.destin = destin;
    edge.weight = weight;
    edge.ramp_id = ramp_id;
    return edge;
}

void dijkstra()
{
    using pii = pair<int, int>;
    priority_queue<pii, vector<pii>, greater<pii>> q;
    for (int i = 0; i < node_count; ++i)
    {
        dist[i] = INT32_MAX;
    }
    dist[0] = 0;
    q.push({0, 0});
    while (!q.empty())
    {
        int node_id = q.top().second;
        int distance = q.top().first;
        q.pop();
        if (distance != dist[node_id])
        {
            continue;
        }
        for (auto edge : nodes[node_id].adjs)
        {
            int dest = edge.destin;
            int weight = edge.weight;

            if (dist[dest] > dist[node_id] + weight)
            {
                dist[dest] = dist[node_id] + weight;
                q.push({dist[dest], dest});
                nodes[dest].to_edge = edge;
            }
        }
    }
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin >> n >> L;
    nodes[0] = {0};
    nodes[1] = {L};
    node_ids.push_back({0, (int)node_ids.size()});
    node_ids.push_back({L, (int)node_ids.size()});

    int x, d, t, p;
    for (int i = 0; i < n; ++i)
    {
        cin >> x >> d >> t >> p;
        d += x;
        x -= p;
        t += p;
        if (x < 0)
        {
            continue;
        }

        Edge ramp = create_edge((int)node_ids.size(), (int)node_ids.size() + 1, t, i + 1);
        Node node = {x};
        node.adjs.push_back(ramp);
        nodes[(int)node_ids.size()] = node;
        node_ids.push_back({x, (int)node_ids.size()});

        nodes[(int)node_ids.size()].coord = d;
        node_ids.push_back({d, (int)node_ids.size()});
    }


    sort(node_ids.begin(), node_ids.end());
    node_count = (int)node_ids.size();
    for (int i = 0; i < node_count - 1; ++i)
    {
        int i_index = node_ids[i].index;
        int ii_index = node_ids[i + 1].index;
        nodes[i_index].adjs.push_back(create_edge(i_index, ii_index, nodes[ii_index].coord - nodes[i_index].coord, 0));
        nodes[ii_index].adjs.push_back(create_edge(ii_index, i_index, nodes[ii_index].coord - nodes[i_index].coord, 0));
    }

    dijkstra();
    cout << dist[1] << endl;

    int current = 1;
    vector<int> used_ramps;
    while (current > 0)
    {
        if (nodes[current].to_edge.ramp_id > 0)
        {
            used_ramps.push_back(nodes[current].to_edge.ramp_id);
        }
        current = nodes[current].to_edge.source;
    }
    cout << used_ramps.size() << endl;
    for (int i = used_ramps.size() - 1; i >= 0; --i)
    {
        cout << used_ramps[i] << " ";
    }
    cout << endl;

    return 0;
}