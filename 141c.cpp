#include <cmath>
#include <iostream>
#include <algorithm>
using namespace std;
typedef long long ll;

int i, j, k;
ll max_num = 1000000000;
struct person
{
    char name[10];
    int ai;
};

bool custom_compare(const person p1, const person p2)
{
    return p1.ai < p2.ai;
}

int main()
{
    ios_base::sync_with_stdio(false);
    int n;
    cin >> n;
    person a[n];
    for (i = 0; i < n; ++i)
    {
        cin >> a[i].name >> a[i].ai;
    }

    sort(a, a + (sizeof(a) / sizeof(a[0])), custom_compare);
    for (i = 0; i < n; ++i)
    {
        if (a[i].ai > i)
        {
            cout << -1 << endl;
            return 0;
        }
    }

    int step = max_num / n;
    ll possible_values[n];
    for (i = 0; i < n; ++i)
    {
        possible_values[i] = (ll)max_num - (ll)(step * i);
    }
    ll h[n];
    for (i = n - 1; i >= 0; --i)
    {
        int additional_step = 0;
        if (i < n - 1)
        {
            j = i;
            while (j < n - 1 && a[j].ai == a[j + 1].ai)
            {
                additional_step++;
                j += 1;
            }
        }
        j = a[i].ai == 0 ? j = additional_step : j = a[i].ai + additional_step;
        while (true)
        {
            if (possible_values[j] != 0)
            {
                h[i] = possible_values[j];
                possible_values[j] = 0;
                break;
            }
            ++j;
        }
    }

    for (i = 0; i < n; ++i)
    {
        cout << a[i].name << ' ' << h[i] << endl;
    }

    return 0;
}