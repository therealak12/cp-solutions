// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 1005;
int n;
int nums[MAXN];

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n;
        for (int i = 0; i < n; i++)
        {
            cin >> nums[i];
        }
        int total[2] = {0};
        int prev[2] = {0};
        int curr[2] = {0};
        int lp = 0;
        int rp = n - 1;
        bool lturn = true;
        int turns = 0;
        while (lp < rp + 1)
        {
            turns += 1;
            if (lturn)
            {
                curr[0] = 0;
                while (curr[0] <= curr[1] && lp < rp + 1)
                {
                    curr[0] += nums[lp];
                    total[0] += nums[lp];
                    lp += 1;
                }
            }
            else
            {
                curr[1] = 0;
                while (curr[1] <= curr[0] && lp < rp + 1)
                {
                    curr[1] += nums[rp];
                    total[1] += nums[rp];
                    rp -= 1;
                }
            }
            lturn = !lturn;
        }
        cout << turns << " " << total[0] << " " << total[1] << "\n";
    }

    return 0;
}