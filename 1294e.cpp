#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n, m;
vvi nums;

int main()
{
    FASTER;
    cin >> n >> m;
    nums.resize(n);
    for (int i = 0; i < n; i++)
    {
        nums[i].resize(m);
        for (int j = 0; j < m; j++)
        {
            cin >> nums[i][j];
            nums[i][j] -= 1;
        }
    }

    int ans = 0;
    for (int j = 0; j < m; j++)
    {
        vi offset_count(n, 0);
        int min_cost = n;
        for (int i = 0; i < n; i++)
        {
            if (nums[i][j] % m == j && nums[i][j] / m >= 0 && nums[i][j] / m <= n - 1)
            {
                int position = (nums[i][j] - j) / m;
                int offset = position <= i ? i - position : n - (position - i);
                offset_count[offset] += 1;
                if ((n - offset_count[offset]) + offset < min_cost)
                {
                    min_cost = (n - offset_count[offset]) + offset;
                }
            }
        }
        ans += min_cost;
    }

    cout << ans << "\n";

    return 0;
}