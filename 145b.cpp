#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define pii pair<int, int>

int four, seven, four_seven, seven_four;

bool inadequate_numbers()
{
    return four_seven > seven || four_seven > four || seven_four > seven || seven_four > four ||
           ((four_seven + seven_four) > (four + seven - 1));
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> four >> seven >> four_seven >> seven_four;
    if (abs(seven_four - four_seven) > 1 || inadequate_numbers())
    {
        cout << "-1" << endl;
        return 0;
    }
    if (four_seven > seven_four)
    {
        for (; four > four_seven; --four)
        {
            cout << "4";
        }
        for (int i = 0; i < four_seven; ++i)
        {
            cout << "47";
        }
        four -= four_seven;
        seven -= four_seven;
        for (; seven > 0; --seven)
        {
            cout << "7";
        }
    }
    else if (seven_four > four_seven)
    {
        cout << "7";
        seven -= 1;
        for (; four >= seven_four; --four)
        {
            cout << "4";
        }
        seven_four -= 1;
        for (; seven_four > 1; --seven_four)
        {
            cout << "74";
            seven -= 1;
        }
        for (; seven > 0; --seven)
        {
            cout << "7";
        }
        cout << "4";
    }
    else
    {
        if (four == four_seven)
        {
            cout << "7";
            seven -= 1;
            for (; four_seven > 0; --four_seven)
            {
                cout << "47";
            }
            while (seven > four)
            {
                cout << "7";
                seven -= 1;
            }
        }
        else if (seven == four_seven)
        {
            while (four > seven)
            {
                cout << "4";
                four -= 1;
            }
            for (; four_seven > 0; --four_seven)
            {
                cout << "74";
            }
        }
        else
        {
            for (; four > (four_seven + 1); --four)
            {
                cout << "4";
            }
            for (; four_seven > 0; --four_seven)
            {
                cout << "47";
            }
            for (; seven > seven_four; --seven)
            {
                cout << "7";
            }
            cout << "4";
        }
    }
    cout << endl;
    return 0;
}