// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

int sigma(int n)
{
    if (n == 0)
    {
        return 0;
    }
    return (n * (n + 1)) / 2;
}

int n;

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n;
        int ans = 0;
        for (int i = 1; i <= n && n >= 2; i++)
        {
            if (2 * sigma(i) + sigma(i - 1) >= n) {
                if (2 * sigma(i) + sigma(i - 1) == n) {
                    n -= 2 * sigma(i) + sigma(i - 1);
                } else {
                    n -= 2 * sigma(i - 1) + sigma(i - 2);
                }
                i = 1;
                ans += 1;
            }
        }
        cout << ans << "\n";
    }

    return 0;
}