#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)

int global_ans = -1;
int n, m;
vector<char> points;
vector<char> final_points;

void print_points(vector<char> to_print = final_points)
{
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < m; ++j)
		{
			cout << to_print[i * m + j];
		}
		cout << endl;
	}
}

vector<int> find_left_l(int i, int j)
{
	vector<int> l;
	int first = i * m + j;
	l.push_back(first);
	l.push_back(first + 1);
	l.push_back(first + 2);
	l.push_back(first + 2 - m);
	l.push_back(first + 2 + m);
	return l;
}

vector<int> find_right_l(int i, int j)
{
	vector<int> l;
	int first = i * m + j;
	l.push_back(first);
	l.push_back(first + m);
	l.push_back(first + 2 * m);
	l.push_back(first + m + 1);
	l.push_back(first + m + 2);
	return l;
}

vector<int> find_up_l(int i, int j)
{
	vector<int> l;
	int first = i * m + j;
	l.push_back(first);
	l.push_back(first + m);
	l.push_back(first + 2 * m);
	l.push_back(first + 2 * m + 1);
	l.push_back(first + 2 * m - 1);
	return l;
}

vector<int> find_down_l(int i, int j)
{
	vector<int> l;
	int first = i * m + j;
	l.push_back(first);
	l.push_back(first + 1);
	l.push_back(first + 2);
	l.push_back(first + m + 1);
	l.push_back(first + 2 * m + 1);
	return l;
}

void fill(vector<int> to_fill, char to_insert)
{
	for (auto point : to_fill)
	{
		points[point] = to_insert;
	}
}

bool is_clear(vector<int> to_look)
{
	for (auto point : to_look)
	{
		if (points[point] != '.')
		{
			return false;
		}
	}
	return true;
}

void calc(int first_point, char to_insert, int t_count, int remaining_points)
{
	for (int index = first_point; index < points.size(); ++index)
	{
		int possible_ts = (remaining_points / 5) + t_count;

		if (possible_ts <= global_ans)
		{
			return;
		}
		int i = index / m;
		int j = (index % m);
		if (points[i * m + j] != '.')
		{
			continue;
		}

		vector<int> left_l = find_left_l(i, j);
		vector<int> right_l = find_right_l(i, j);
		vector<int> up_l = find_up_l(i, j);
		vector<int> down_l = find_down_l(i, j);

		if (i + 1 < n && i - 1 >= 0 && j + 2 < m && is_clear(left_l))
		{
			fill(left_l, to_insert);
			calc(index + 2, to_insert + 1, t_count + 1, remaining_points - 5);
			if (t_count + 1 > global_ans)
			{
				final_points = points;
				global_ans = t_count + 1;
			}
			fill(left_l, '.');
		}
		if (i + 2 < n && j + 2 < m && is_clear(right_l))
		{
			fill(right_l, to_insert);
			calc(index + 1, to_insert + 1, t_count + 1, remaining_points - 5);
			if (t_count + 1 > global_ans)
			{
				final_points = points;
				global_ans = t_count + 1;
			}
			fill(right_l, '.');
		}
		if (i + 2 < n && j - 1 >= 0 && j + 1 < m && is_clear(up_l))
		{
			fill(up_l, to_insert);
			calc(index + 1, to_insert + 1, t_count + 1, remaining_points - 5);
			if (t_count + 1 > global_ans)
			{
				final_points = points;
				global_ans = t_count + 1;
			}
			fill(up_l, '.');
		}
		if (i + 2 < n && j + 2 < m && is_clear(down_l))
		{
			fill(down_l, to_insert);
			calc(index + 1, to_insert + 1, t_count + 1, remaining_points - 5);
			if (t_count + 1 > global_ans)
			{
				final_points = points;
				global_ans = t_count + 1;
			}
			fill(down_l, '.');
		}
		remaining_points -= 1;
	}
}

int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);

	cin >> n >> m;
	if (n <= 2 || m <= 2)
	{
		cout << "0" << endl;
		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < m; ++j)
			{
				cout << ".";
			}
			cout << endl;
		}
		return 0;
	}
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < m; ++j)
		{
			points.push_back('.');
		}
	}

	calc(0, 'A', 0, points.size());
	cout << global_ans << endl;
	print_points();

	return 0;
}
