// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 1000005;
const int MAXK = 1000000005;
int n, k;

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n >> k;
        while (n % 2 != 0)
        {
            k -= 1;
            for (int i = 2; i <= n; i++)
            {
                if (n % i == 0)
                {
                    n = n + i;
                    break;
                }
            }
        }
        n = n + (k * 2);
        cout << n << "\n";
    }

    return 0;
}