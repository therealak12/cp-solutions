#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 100005;
int n;
int nums[MAXN];
int pre_g[MAXN];
int suf_g[MAXN];

void calc_suf_g()
{
    for (int i = n - 1; i >= 0; i--)
    {
        if (i == n - 1)
        {
            suf_g[i] = nums[i];
        }
        else
        {
            suf_g[i] = __gcd(nums[i], suf_g[i + 1]);
        }
    }
}

ll find_lcm(ll a, ll b)
{
    return a * b / (ll)__gcd(a, b);
}

int main()
{
    FASTER;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> nums[i];
        if (i == 0)
        {
            pre_g[i] = nums[i];
        }
        else
        {
            pre_g[i] = __gcd(nums[i], pre_g[i - 1]);
        }
    }
    calc_suf_g();
    
    ll ans = find_lcm(suf_g[1], pre_g[n - 2]);
    for (int i = 1; i <= n - 2; i++)
    {
        ans = find_lcm(ans, __gcd(pre_g[i - 1], suf_g[i + 1]));
    }
    cout << ans << "\n";

    return 0;
}