// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n0, n1, n2;
int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n0 >> n1 >> n2;
        if (n0 > 0)
        {
            for (int i = 0; i <= n0; i++)
            {
                cout << "0";
            }
        }
        if (n1 + n2 <= 0)
        {
            cout << "\n";
        }
        else
        {
            if (n1 > 0 && n0 == 0) {
                cout << 0;
            }
            for (int i = 0; i <= n2; i++)
            {
                cout << 1;
            }
            n1 -= 1;
            for (int i = 0; i < n1; i++)
            {
                cout << i % 2;
            }
            cout << "\n";
        }
    }

    return 0;
}