#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER                    \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0)

int n;
vector<int> numbers;
vvi dp;

int main()
{
    FASTER;

    cin >> n;
    numbers.resize(n + 1);
    for (int i = 0; i < n; i++)
    {
        cin >> numbers[i];
    }
    numbers[n] = 0;
    dp.resize(n + 1);
    for (int i = 0; i < n + 1; i++)
    {
        dp[i].resize(n + 1);
    }

    for (int i = 0; i < n; i++)
    {
        dp[i][i] = 1;
        dp[i][n] = 0;
        dp[n][i] = 0;
    }
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            dp[i][j] = 0;
        }
    }

    // for (int i = 0; i <= n; i++)
    // {
    //     for (int j = 0; j <= n; j++)
    //     {
    //         cout << dp[i][j] << " ";
    //     }
    //     cout << endl;
    // }

    for (int i = n - 2; i >= 0; i--)
    {
        for (int j = i + 1; j < n; j++)
        {
            dp[i][j] = 1 + dp[i + 1][j];
            if (i < n - 1 && numbers[i] == numbers[i + 1])
            {
                dp[i][j] = min(dp[i][j], 1 + dp[i + 2][j]);
            }
            for (int k = i + 2; k <= j; k++)
            {
                if (numbers[k] == numbers[i])
                {
                    dp[i][j] = min(dp[i][j], dp[i + 1][k - 1] + dp[k + 1][j]);
                }
            }
        }
    }

    cout << dp[0][n - 1] << endl;

    return 0;
}