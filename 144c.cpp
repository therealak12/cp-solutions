#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)

const int ALPHA_COUNT = 26;

string s, p;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin >> s;
    cin >> p;
    if (p.size() > s.size())
    {
        cout << 0 << endl;
        return 0;
    }

    int p_repeats[ALPHA_COUNT];
    int s_repeats[ALPHA_COUNT];
    for (int i = 0; i < ALPHA_COUNT; ++i)
    {
        p_repeats[i] = s_repeats[i] = 0;
    }

    for (auto c : p)
    {
        p_repeats[c - 'a'] += 1;
    }

    int start, end;
    start = 0;
    end = p.size() - 1;
    int reserve_count = 0;
    for (int i = 0; i <= end; ++i)
    {
        if (s[i] != '?')
        {
            s_repeats[s[i] - 'a'] += 1;
        }
        else
        {
            reserve_count += 1;
        }
    }

    int ans = 0;
    while (end < s.size())
    {
        bool is_anagram = true;
        int reserve_need = 0;
        for (int i = 0; i < ALPHA_COUNT; ++i)
        {
            if (p_repeats[i] < s_repeats[i])
            {
                is_anagram = false;
                break;
            }
            reserve_need += p_repeats[i] - s_repeats[i];
        }
        if (is_anagram && reserve_count == reserve_need)
        {
            ans += 1;
        }
        if (s[start] == '?')
        {
            reserve_count -= 1;
        }
        else
        {
            s_repeats[s[start] - 'a'] -= 1;
        }
        if (s[end + 1] == '?')
        {
            reserve_count += 1;
        }
        else
        {
            s_repeats[s[end + 1] - 'a'] += 1;
        }

        start += 1;
        end += 1;
    }

    cout << ans << endl;

    return 0;
}