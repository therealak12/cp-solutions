// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>
#include <stack>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 200005;
int n;
vvi graph(MAXN);
vector<bool> visited(MAXN, false);
vi parents(MAXN, -1);

void dfs(int source)
{
    // cout << "alarm " << source << "\n";
    visited[source] = true;
    for (int neighbour : graph[source])
    {
        if (!visited[neighbour])
        {
            parents[neighbour] = source;
            dfs(neighbour);
        }
    }
}

int main()
{
    FASTER;
    cin >> n;
    for (int i = 0; i < n - 1; i++)
    {
        int parent;
        cin >> parent;
        parent -= 1;
        graph[parent].push_back(i + 1);
    }

    dfs(0);
    
    int current_node = n - 1;
    stack<int> path;
    while (true)
    {
        path.push(current_node);
        if (current_node == 0)
        {
            break;
        }
        current_node = parents[current_node];
    }

    while (!path.empty())
    {
        cout << path.top() + 1 << " ";
        path.pop();
    }
    cout << "\n";

    return 0;
}