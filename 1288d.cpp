#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n, m;
vvi nums;
vector<int> corr_arrays;
int arr1 = -1;
int arr2 = -1;

void reset_arrays()
{
    for (int i = 0; i < (1 << m); i++)
    {
        corr_arrays[i] = -1;
    }
}

bool greater_or_equal(int mid)
{
    reset_arrays();

    for (int i = 0; i < n; i++)
    {
        int mask_index = 0;
        for (int j = 0; j < m; j++)
        {
            if (nums[i][j] >= mid)
            {
                mask_index ^= (1 << j);
            }
        }
        corr_arrays[mask_index] = i;
    }

    for (int i = 0; i < (1 << m); i++)
    {
        for (int j = i; j < (1 << m); j++)
        {
            if (((i | j) == (1 << m) - 1) && corr_arrays[i] != -1 && corr_arrays[j] != -1)
            {
                arr1 = corr_arrays[i] + 1;
                arr2 = corr_arrays[j] + 1;
                return true;
            }
        }
    }
    return false;
}

int main()
{
    FASTER;
    cin >> n >> m;
    nums.resize(n);
    corr_arrays.resize(1 << m);
    for (int i = 0; i < n; i++)
    {
        nums[i].resize(m);
        for (int j = 0; j < m; j++)
        {
            cin >> nums[i][j];
        }
    }

    int left = 0;
    int right = 1e9 + 2;
    int mid = (left + right) / 2;
    while (left + 1 < right)
    {
        if (greater_or_equal(mid))
        {
            left = mid;
        }
        else
        {
            right = mid;
        }
        mid = (left + right) / 2;
    }

    if (arr1 == -1)
    {
        arr1 = 1;
    }
    if (arr2 == -1)
    {
        arr2 = 1;
    }
    cout << arr1 << " " << arr2 << "\n";

    return 0;
}