#include <bits/stdc++.h>

using namespace std;
typedef long long ll;
#define mp(a, b) make_pair(a, b)

struct Edge
{
    int x;
    int y;
    int edge_id;
    ;
};

int n, m;
const int maxm = 1e5;
vector<Edge> s_edges;
vector<Edge> m_edges;
vector<Edge> edges;

void init_dsu(int parents[], int ranks[])
{
    for (int i = 0; i < n; ++i)
    {
        parents[i] = i;
        ranks[i] = 0;
    }
}

int find_set(int x, int parents[])
{
    if (x == parents[x])
    {
        return x;
    }
    parents[x] = find_set(parents[x], parents);
    return parents[x];
}

void union_sets(int x, int y, int parents[], int ranks[])
{
    x = find_set(x, parents);
    y = find_set(y, parents);
    if (x != y)
    {
        if (ranks[x] < ranks[y])
        {
            swap(x, y);
        }
        parents[y] = x;
        if (ranks[x] == ranks[y])
        {
            ranks[x] += 1;
        }
    }
}

int count_m_components(int parents[], int ranks[])
{
    unordered_set<int> m_components;
    for (int i = 0; i < n; ++i)
    {
        m_components.insert(parents[i]);
    }
    return m_components.size();
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin >> n >> m;
    int x, y;
    char p;
    for (int i = 0; i < m; ++i)
    {
        cin >> x >> y >> p;
        x -= 1;
        y -= 1;
        // Did nothing for self-loops, i.e. v-v edges
        Edge edge = {x, y, i};
        if (p == 'S')
        {
            s_edges.push_back(edge);
        }
        else
        {
            m_edges.push_back(edge);
        }
        edges.push_back(edge);
    }

    int parents[n];
    int ranks[n];
    init_dsu(parents, ranks);
    if (n % 2 == 0)
    {
        cout << -1 << endl;
        return 0;
    }

    for (auto edge : m_edges)
    {
        union_sets(edge.x, edge.y, parents, ranks);
    }
    int cnt = count_m_components(parents, ranks);
    if (cnt - 1 > (n - 1) / 2)
    {
        cout << -1 << endl;
        return 0;
    }

    vector<int> ans;
    for (auto edge : s_edges)
    {
        if (find_set(edge.x, parents) != find_set(edge.y, parents))
        {
            union_sets(edge.x, edge.y, parents, ranks);
            ans.push_back(edge.edge_id);
        }
        if (ans.size() == (n - 1) / 2)
        {
            break;
        }
    }
    init_dsu(parents, ranks);
    for (auto edge_id : ans)
    {
        Edge edge = edges[edge_id];
        union_sets(edge.x, edge.y, parents, ranks);
    }
    for (auto edge : s_edges)
    {
        if (find_set(edge.x, parents) != find_set(edge.y, parents))
        {
            union_sets(edge.x, edge.y, parents, ranks);
            ans.push_back(edge.edge_id);
        }
        if (ans.size() == (n - 1) / 2)
        {
            break;
        }
    }
    if (ans.size() < (n - 1) / 2)
    {
        cout << -1 << endl;
        return 0;
    }
    for (auto edge : m_edges)
    {
        if (find_set(edge.x, parents) != find_set(edge.y, parents))
        {
            union_sets(edge.x, edge.y, parents, ranks);
            ans.push_back(edge.edge_id);
        }
    }

    cout << ans.size() << endl;
    for (auto edge_id : ans)
    {
        cout << edge_id + 1 << " ";
    }
    cout << endl;

    return 0;
}