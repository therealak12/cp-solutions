// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>
#include <stack>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

struct comapre_intervals
{
    bool operator() (const pii &lhs, const pii &rhs) const
    {
        if (lhs.second - lhs.first != rhs.second - rhs.first)
        {
            return lhs.second - lhs.first > rhs.second - rhs.first;
        }
        return lhs.first <= rhs.first;
    }
};

const int MAXN = 200005;
int n;
int nums[MAXN];
set<pii, comapre_intervals> intervals;

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n;
        intervals.insert(mp(0, n - 1));
        int i = 1;
        while (!intervals.empty())
        {
            pii curr = *(intervals.begin());
            intervals.erase(intervals.begin());
            int mid = (curr.first + curr.second) / 2;
            nums[mid] = i;
            i += 1;
            if (curr.first == curr.second - 1)
            {
                intervals.insert(mp(curr.second, curr.second));
            }
            else if (curr.first < curr.second - 1)
            {
                intervals.insert(mp(curr.first, mid - 1));
                intervals.insert(mp(mid + 1, curr.second));
            }
        }
        for (int i = 0; i < n; i++)
        {
            cout << nums[i] << " ";
        }
        cout << "\n";
    }

    return 0;
}