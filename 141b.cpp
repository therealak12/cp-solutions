#include <iostream>
#include <cmath>
using namespace std;

int i, j, k;
int a, x, y;
int main()
{
    ios_base::sync_with_stdio(false);
    cin >> a >> x >> y;

    if (x > -((a + 1) / 2) && x < (a + 1) / 2)
    {
        if (y > 0 && y < a)
        {
            cout << 1 << endl;
            return 0;
        }
        else if (y > a && y < 2 * a)
        {
            cout << 2 << endl;
            return 0;
        }
    }

    for (i = 2; i <= y; ++i)
    {
        if (y > i * a && y < (i + 1) * a)
        {
            if (i % 2 == 0)
            {
                if (x > -a && x < 0)
                {
                    cout << (i / 2) * 3 << endl;
                    return 0;
                }
                else if (x > 0 && x < a)
                {
                    cout << ((i / 2) * 3) + 1 << endl;
                    return 0;
                }
            }
            else if (x > -((a + 1) / 2) && x < (a + 1) / 2)
            {
                cout << (((i + 1) / 2) * 3) - 1 << endl;
                return 0;
            }
        }
    }

    cout << -1 << endl;
    return 0;
}