#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 100005;
int n, k;
int nums[MAXN];

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n >> k;
        bool k_exists = false;
        for (int i = 0; i < n; i++)
        {
            cin >> nums[i];
            if (nums[i] == k)
            {
                k_exists = true;
            }
        }
        if (!k_exists)
        {
            cout << "no\n";
            continue;
        }
        if (n == 1)
        {
            cout << "yes\n";
            continue;
        }
        if (n == 2)
        {
            if (nums[0] >= k && nums[1] >= k)
            {
                cout << "yes\n";
            }
            else
            {
                cout << "no\n";
            }
            continue;
        }
        bool possible = false;
        for (int i = 2; i < n; i++)
        {
            if (nums[i - 2] >= k && nums[i - 1] >= k ||
                nums[i - 2] >= k && nums[i] >= k ||
                nums[i - 1] >= k && nums[i] >= k)
            {
                cout << "yes\n";
                possible = true;
                break;
            }
        }
        if (!possible)
        {
            cout << "no\n";
        }
    }

    return 0;
}