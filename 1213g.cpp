#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
const int MAXN = 200005;

struct Edge
{
    int u;
    int v;
    int w;
};

struct Query
{
    int value;
    int index;
};

int n, m;
vector<Edge> edges(MAXN);
vi parents(MAXN);
vector<Query> queries(MAXN);
vi ans(MAXN, 0);
vi ranks(MAXN, 1);

bool compare_edge(const Edge &lhs, const Edge &rhs)
{
    return lhs.w < rhs.w;
}

bool compare_query(const Query &lhs, const Query &rhs)
{
    return lhs.value < rhs.value;
}

int find_parent(int node)
{
    if (node == parents[node])
    {
        return node;
    }
    else
    {
        parents[node] = find_parent(parents[node]);
        return parents[node];
    }
}

void merge_sets(int u, int v)
{
    u = find_parent(u);
    v = find_parent(v);
    if (ranks[v] < ranks[u])
    {
        swap(u, v);
    }
    ranks[v] += ranks[u];
    parents[u] = v;
}

int main()
{
    FASTER;
    cin >> n >> m;

    for (int i = 0; i < n - 1; i++)
    {
        int u, v, w;
        cin >> u >> v >> w;
        u -= 1;
        v -= 1;
        edges[i] = {u, v, w};
    }

    for (int i = 0; i < m; i++)
    {
        cin >> queries[i].value;
        queries[i].index = i;
    }

    sort(queries.begin(), queries.begin() + m, &compare_query);
    sort(edges.begin(), edges.begin() + n - 1, &compare_edge);
    for (int i = 0; i < n; i++)
    {
        parents[i] = i;
    }

    for (int i = 0; i < m; i++)
    {
        int j;
        for (j = 0; j < n - 1 && edges[j].w < queries[i].value; j++)
        {
            merge_sets(edges[j].u, edges[j].v);
        }
        ans[queries[i].index] = ranks[find_parent(edges[j].u)] * ranks[find_parent(edges[j].v)];
    }

    for (int i = 0; i < m; i++)
    {
        cout << ans[i] << " ";
    }
    cout << "\n";

    return 0;
}