// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>
#include <stack>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 30005;
int n, t;
vi destinations(MAXN, -1);
vector<bool> visited(MAXN, false);

int main()
{
    FASTER;
    cin >> n >> t;
    t -= 1;
    for (int i = 0; i < n - 1; i++)
    {
        int ai;
        cin >> ai;
        destinations[i] = ai + i;
    }
    int current_node = 0;
    while (true)
    {
        current_node = destinations[current_node];
        if (current_node > t)
        {
            cout << "NO\n";
            return 0;
        }
        if (current_node == t)
        {
            cout << "YES\n";
            return 0;
        }
    }

    return 0;
}