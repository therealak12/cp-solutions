// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 200005;
int n;
vvi graph(MAXN);
ll ans = 0;
ll max_ans = 0;

void dfs(int root, vector<bool> vis, int height)
{
    vis[root] = true;
    ans += height;
    for (int neighb : graph[root])
    {
        if (!vis[neighb])
        {
            dfs(neighb, vis, height + 1);
        }
    }
}

int main()
{
    FASTER;
    cin >> n;
    for (int i = 0; i < n - 1; i++)
    {
        int u, v;
        cin >> u >> v;
        u -= 1;
        v -= 1;
        graph[u].push_back(v);
        graph[v].push_back(u);
    }

    for (int i = 0; i < n; i++)
    {
        if (graph[i].size() == 1)
        {
            vector<bool> vis(n, 0);
            dfs(i, vis, 1);
            max_ans = max(ans, max_ans);
            ans = 0;
        }
    }
    cout << max_ans << "\n";

    return 0;
}