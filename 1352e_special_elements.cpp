// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 8005;
int n;
int nums[MAXN];
vector<bool> sums(MAXN, false);
int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n;
        for (int i = 0; i < n; i++)
        {
            cin >> nums[i];
        }
        sums.assign(n + 1, false);
        for (int i = 0; i < n - 1; i++)
        {
            int sum = nums[i];
            for (int j = i + 1; j < n; j++)
            {
                sum += nums[j];
                if (sum > n) {
                    break;
                }
                sums[sum] = true;
            }
        }
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            if (sums[nums[i]]) {
                ans += 1;
            }
        }
        cout << ans << "\n";
    }

    return 0;
}