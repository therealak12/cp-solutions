#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)

struct Radiator
{
    int x;
    int y;
    int r;
};

int xa, xb, ya, yb;
int n;
vector<Radiator> radiators;

int calc_dist(Radiator radiator, int x, int y)
{
    return ceil(sqrt(pow(x - radiator.x, 2) + pow(y - radiator.y, 2)));
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> xa >> ya >> xb >> yb;
    if (xa > xb)
    {
        swap(xa, xb);
    }
    if (ya > yb)
    {
        swap(ya, yb);
    }

    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        Radiator radiator;
        cin >> radiator.x >> radiator.y >> radiator.r;
        radiators.push_back(radiator);
    }

    int ans = 0;
    for (int i = xa; i <= xb; ++i)
    {
        int b1 = 1;
        int b2 = 1;
        for (auto radiator : radiators)
        {
            if (calc_dist(radiator, i, ya) <= radiator.r)
            {
                b1 = 0;
            }
            if (calc_dist(radiator, i, yb) <= radiator.r)
            {
                b2 = 0;
            }
            if (b1 + b2 == 0)
            {
                break;
            }
        }
        ans += b1 + b2;
    }
    for (int i = ya + 1; i < yb; ++i)
    {

        int b1 = 1;
        int b2 = 1;
        for (auto radiator : radiators)
        {
            if (calc_dist(radiator, xa, i) <= radiator.r)
            {
                b1 = 0;
            }
            if (calc_dist(radiator, xb, i) <= radiator.r)
            {
                b2 = 0;
            }
            if (b1 + b2 == 0)
            {
                break;
            }
        }
        ans += b1 + b2;
    }

    cout << ans << endl;
    return 0;
}