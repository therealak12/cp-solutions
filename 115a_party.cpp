// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>
#include <stack>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 2005;
int n;
vi parents(MAXN, -1);
vector<bool> visited(MAXN, false);
int max_height = 0;

void dfs(int source, int height)
{
    visited[source] = true;
    max_height = max(max_height, height);
    for (int i = 0; i < n; i++)
    {
        if (parents[i] == source && !visited[i])
        {
            dfs(i, height + 1);
        }
    }
}

int main()
{
    FASTER;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        int pi;
        cin >> pi;
        if (pi != -1)
        {
            pi -= 1;
            parents[i] = pi;
        }
    }

    for (int i = 0; i < n; i++)
    {
        if (!visited[i] && parents[i] == -1)
        {
            dfs(i, 1);
        }
    }
    cout << max_height << "\n";

    return 0;
}