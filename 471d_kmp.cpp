#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n, w;
vi eleph, bear, lps;

void calc_lps()
{
    lps[0] = 0;
    int i = 1;
    int len = 0;
    while (i < w)
    {
        if (eleph[i] == eleph[len])
        {
            len += 1;
            lps[i] = len;
            i += 1;
        }
        else
        {
            if (len != 0)
            {
                len = lps[len - 1];
            }
            else
            {
                lps[i] = 0;
                i += 1;
            }
        }
    }
}

int main()
{
    FASTER;
    cin >> n >> w;
    bear.resize(n);
    eleph.resize(w);
    lps.resize(w);
    for (int i = 0; i < n; i++)
    {
        cin >> bear[i];
        if (i > 0)
        {
            bear[i - 1] = bear[i - 1] - bear[i];
        }
    }
    for (int i = 0; i < w; i++)
    {
        cin >> eleph[i];
        if (i > 0)
        {
            eleph[i - 1] = eleph[i - 1] - eleph[i];
        }
    }

    if (w == 1)
    {
        cout << n << "\n";
        return 0;
    }

    w -= 1;
    n -= 1;
    calc_lps();
    int ans = 0;
    int match_length = 0;
    int i = 0;
    while (i < n)
    {
        if (eleph[match_length] == bear[i])
        {
            match_length += 1;
            i += 1;
            if (match_length == w)
            {
                ans += 1;
                match_length = lps[match_length - 1];
            }
        }
        else
        {
            if (match_length == 0)
            {
                i += 1;
            }
            else
            {
                match_length = lps[match_length - 1];
            }
        }
    }
    cout << ans << "\n";

    return 0;
}