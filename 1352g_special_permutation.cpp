// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

int n;
const int MAXN = 1005;

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n;
        if (n < 4) {
            cout << "-1\n";
            continue;
        }
        if (n % 2 == 1)
        {
            for (int i = n; i >= 1; i -= 2)
            {
                cout << i << " ";
            }
            cout << "4 2 ";
            for (int i = 6; i <= n; i += 2)
            {
                cout << i << " ";
            }
            cout << "\n";
        }
        else
        {
            for (int i = n - 1; i >= 1; i -= 2)
            {
                cout << i << " ";
            }
            cout << "4 2 ";
            for (int i = 6; i <= n; i += 2)
            {
                cout << i << " ";
            }
            cout << "\n";
        }
    }

    return 0;
}