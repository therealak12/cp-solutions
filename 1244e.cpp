#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

const int MAXN = 1e5 + 5;
int n;
ll k;
int nums[MAXN];

int main()
{
    FASTER;
    cin >> n >> k;
    for (int i = 0; i < n; i++)
    {
        cin >> nums[i];
    }
    sort(nums, nums + n);

    int last_inc = 0;
    int last_dec = n - 1;

    while (k > 0)
    {
        while (last_inc + 1 < n && nums[last_inc + 1] == nums[last_inc])
        {
            last_inc += 1;
        }
        while (last_dec > 0 && nums[last_dec] == nums[last_dec - 1])
        {
            last_dec -= 1;
        }
        if (nums[last_dec] <= nums[last_inc])
        {
            break;
        }
        if (last_inc + 1 < n - last_dec)
        {
            if (last_inc + 1 <= k)
            {
                while (nums[last_inc] < nums[last_inc + 1] && last_inc + 1 <= k)
                {
                    k -= last_inc + 1;
                    nums[last_inc] += 1;
                }
            }
            else
            {
                break;
            }
        }
        else if (n - last_dec <= k)
        {
            while (nums[last_dec] > nums[last_dec - 1] && n - last_dec <= k)
            {
                k -= n - last_dec;
                nums[last_dec] -= 1;
            }
        }
        else
        {
            break;
        }
    }
    cout << nums[last_dec] - nums[last_inc] << "\n";

    return 0;
}