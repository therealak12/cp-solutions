#include <bits/stdc++.h>

using namespace std;
typedef long long ll;
#define mp(a, b) make_pair(a, b)

ll calc_rem(ll f1, ll f2, ll f3)
{
	return (f1 + 1) * (f2 + 2) * (f3 + 2) - f1 * f2 * f3;
}

int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);

	ll n;
	cin >> n;
	vector<ll> factors;
	for (ll i = 1; i * i <= n; ++i)
	{
		if (n % i == 0)
		{
			factors.push_back(i);
			factors.push_back(n / i);
		}
	}

	ll maxVal = (n + 1) * 9 - n;
	ll minVal = INT64_MAX;

	for (auto f1 : factors)
	{
		for (auto f2 : factors)
		{
			if (n % (f1 * f2) == 0)
			{
				ll temp = calc_rem(f1, f2, n / (f2 * f1));
				minVal = min(minVal, temp);
			}
		}
	}

	cout << minVal << " " << maxVal << endl;

	return 0;
}
