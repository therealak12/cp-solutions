#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER                    \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0)

const int MAX_N = 1001;
const int MAX_K = 6;
int n, k;
int indexes[MAX_K][MAX_N];
int numbers[MAX_K][MAX_N];
int lcs[MAX_N];

int main()
{
    FASTER;
    cin >> n >> k;

    for (int i = 0; i < k; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cin >> numbers[i][j];
            numbers[i][j] -= 1;
            indexes[i][numbers[i][j]] = j;
        }
    }

    for (int i = 0; i < n; i++)
    {
        int max_lcs = 0;
        for (int j = 0; j < i; j++)
        {
            int arr_num = 1;
            while (arr_num < k && indexes[arr_num][numbers[0][j]] < indexes[arr_num][numbers[0][i]])
            {
                arr_num += 1;
            }
            if (arr_num == k && lcs[j] > max_lcs)
            {
                max_lcs = lcs[j];
            }
        }
        lcs[i] = max_lcs + 1;
    }

    int ans = 0;
    for (int i = 0; i < n; i++)
    {
        ans = max(ans, lcs[i]);
    }
    cout << ans << endl;

    return 0;
}