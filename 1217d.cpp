#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n, m;
vector<vector<pii>> edges;
vi colors;
vi vis;
int cc = 1;

void dfs(int index)
{
    vis[index] = 1;
    for (pii edge : edges[index])
    {
        if (vis[edge.second] == 0)
        {
            dfs(edge.second);
            colors[edge.first] = 1;
        }
        else if (vis[edge.second] == 1)
        {
            colors[edge.first] = 2;
            cc = 2;
        }
        else if (vis[edge.second] == 2)
        {
            colors[edge.first] = 1;
        }
    }
    vis[index] = 2;
}

int main()
{
    FASTER;
    cin >> n >> m;
    edges.resize(n);
    vis.resize(n);
    colors.resize(m);
    for (int i = 0; i < m; i++)
    {
        int u, v;
        cin >> u >> v;
        u -= 1;
        v -= 1;
        edges[u].push_back(mp(i, v));
    }

    for (int i = 0; i < n; i++)
    {
        if (vis[i] == 0)
        {
            dfs(i);
        }
    }

    cout << cc << "\n";
    for (int i = 0; i < m; i++)
    {
        cout << colors[i] << " ";
    }
    cout << "\n";

    return 0;
}