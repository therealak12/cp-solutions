#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        int n;
        cin >> n;
        int ncopy = n;
        int digit_count = 0;
        while (ncopy != 0)
        {
            digit_count += 1;
            ncopy /= 10;
        }
        vi ans;
        for (int i = 0; i < digit_count; i++)
        {
            while (n % 10 == 0 && n != 0)
            {
                i += 1;
                n /= 10;
            }
            ans.push_back((n % 10) * (int)pow(10, i));
            n /= 10;
        }
        cout << ans.size() << "\n";
        for (int number : ans) {
            cout << number << " ";
        }
        cout << '\n';
    }

    return 0;
}