#include <bits/stdc++.h>

using namespace std;
using ll = long long;
using pii = pair<int, int>;

#define mp(a, b) make_pair(a, b)

vector<pii> bad_neighbs(pii location)
{
    vector<pii> bad_neighbs;
    int x = location.first;
    int y = location.second;
    bad_neighbs.push_back(mp(x + 2, y + 1));
    bad_neighbs.push_back(mp(x + 2, y - 1));
    bad_neighbs.push_back(mp(x + 1, y + 2));
    bad_neighbs.push_back(mp(x + 1, y - 2));
    bad_neighbs.push_back(mp(x - 2, y + 1));
    bad_neighbs.push_back(mp(x - 2, y - 1));
    bad_neighbs.push_back(mp(x - 1, y + 2));
    bad_neighbs.push_back(mp(x - 1, y - 2));
    return bad_neighbs;
}

vector<pii> flat_neighbs(pii location)
{
    vector<pii> neighbs;
    int x = location.first;
    int y = location.second;
    neighbs.push_back(mp(x - 1, y));
    neighbs.push_back(mp(x + 1, y));
    neighbs.push_back(mp(x, y + 1));
    neighbs.push_back(mp(x, y - 1));
    return neighbs;
}

vector<pii> cross_neighbs(pii location)
{
    vector<pii> neighbs;
    int x = location.first;
    int y = location.second;
    neighbs.push_back(mp(x + 1, y + 1));
    neighbs.push_back(mp(x + 1, y - 1));
    neighbs.push_back(mp(x - 1, y + 1));
    neighbs.push_back(mp(x - 1, y - 1));
    return neighbs;
}

bool is_location_valid(pii location, int n, int m)
{
    int x = location.first;
    int y = location.second;
    return x >= 0 && y >= 0 && x < n && y < m;
}

int main()
{
    ios_base::sync_with_stdio(false);
    int n, m;
    cin >> n >> m;
    int locations[n][m];
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            locations[i][j] = 0;
        }
    }

    queue<pii> next_nodes;
    next_nodes.push(mp(0, 0));
    int ans = 0;

    while (!next_nodes.empty())
    {
        pii next_node = next_nodes.front();
        next_nodes.pop();
        if (locations[next_node.first][next_node.second] == 0)
        {
            ans += 1;
            locations[next_node.first][next_node.second] = 2;

            for (auto bad_neighb : bad_neighbs(next_node))
            {
                if (is_location_valid(bad_neighb, n, m))
                {
                    if (locations[bad_neighb.first][bad_neighb.second] == 0)
                    {
                        locations[bad_neighb.first][bad_neighb.second] = 1;
                        next_nodes.push(bad_neighb);
                    }
                }
            }
            if (n == 2 || m == 2)
            {
                for (auto node : flat_neighbs(next_node))
                {
                    if (is_location_valid(node, n, m) && locations[node.first][node.second] == 0)
                    {
                        next_nodes.push(node);
                    }
                }
            }
            for (auto node : cross_neighbs(next_node))
            {
                if (is_location_valid(node, n, m) && locations[node.first][node.second] == 0)
                {
                    next_nodes.push(node);
                }
            }
        }
        if (next_nodes.empty())
        {
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < m; ++j)
                {
                    for (auto node : flat_neighbs(mp(i, j)))
                    {
                        if (is_location_valid(node, n, m) && locations[node.first][node.second] == 0)
                        {
                            next_nodes.push(node);
                        }
                    }
                }
            }
        }
    }

    cout << ans << endl;

    // uncomment to see the map at the end
    // for (int i = 0; i < n; ++i)
    // {
    //     for (int j = 0; j < m; ++j)
    //     {
    //         cout << locations[i][j] << " ";
    //     }
    //     cout << endl;
    // }
    // cout << endl;

    return 0;
}