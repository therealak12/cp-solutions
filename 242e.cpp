#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

struct Node
{
    ll value = 0;
    bool reversed = false;
};

int n;
vi nums;
int m;
vector<Node> trees[21];

int get_left_index(int index)
{
    return 2 * index + 1;
}

int get_right_index(int index)
{
    return 2 * index + 2;
}

void build_tree(int l, int r, int index, vector<Node> &tree, int i)
{
    if (l == r)
    {
        tree[index] = {(nums[l] >> i) & 1, false};
        return;
    }
    int m = (l + r) / 2;
    build_tree(l, m, get_left_index(index), tree, i);
    build_tree(m + 1, r, get_right_index(index), tree, i);
    tree[index].value = tree[get_left_index(index)].value + tree[get_right_index(index)].value;
}

ll sum_tree(int l, int r, int tl, int tr, int index, bool reversed, vector<Node> &tree)
{
    if (l > r)
    {
        return 0;
    }
    if (l == tl && r == tr)
    {
        if (reversed)
        {
            return (r - l + 1) - tree[index].value;
        }
        else
        {
            return tree[index].value;
        }
    }
    if (tree[index].reversed)
    {
        reversed = !reversed;
    }
    int tm = (tl + tr) / 2;
    return sum_tree(l, min(tm, r), tl, tm, get_left_index(index), reversed, tree) +
           sum_tree(max(l, tm + 1), r, tm + 1, tr, get_right_index(index), reversed, tree);
}

void update_tree(int l, int r, int tl, int tr, int index, int value, vector<Node> &tree)
{
    if (l > r)
    {
        return;
    }
    if (l == tl && r == tr)
    {
        tree[index].reversed = !(tree[index].reversed);
        tree[index].value = (r - l + 1) - tree[index].value;
        return;
    }
    int tm = (tl + tr) / 2;
    update_tree(l, min(r, tm), tl, tm, get_left_index(index), value, tree);
    update_tree(max(l, tm + 1), r, tm + 1, tr, get_right_index(index), value, tree);
    int raw_sum = tree[get_left_index(index)].value + tree[get_right_index(index)].value;
    if (tree[index].reversed)
    {
        tree[index].value = (tr - tl + 1) - raw_sum;
    }
    else
    {
        tree[index].value = raw_sum;
    }
}

int main()
{
    FASTER;
    cin >> n;
    nums.resize(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nums[i];
    }
    for (int i = 0; i < 21; ++i)
    {
        trees[i].resize(4 * n);
        build_tree(0, n - 1, 0, trees[i], i);
    }

    cin >> m;
    for (int i = 0; i < m; i++)
    {
        int type;
        cin >> type;
        if (type == 1)
        {
            int l, r;
            cin >> l >> r;
            l -= 1;
            r -= 1;
            ll sum = 0;
            for (int i = 0; i < 21; i++)
            {
                sum += sum_tree(l, r, 0, n - 1, 0, false, trees[i]) * pow(2, i);
            }
            cout << sum << "\n";
        }
        else if (type == 2)
        {
            int l, r, x;
            cin >> l >> r >> x;
            l -= 1;
            r -= 1;

            for (int i = 0; i < 21; i++)
            {
                if ((x >> i) & 1)
                {
                    update_tree(l, r, 0, n - 1, 0, x, trees[i]);
                }
            }
            // for (int i = 0; i < 4 * n; i++)
            // {
            //     cout << trees[0][i].value << " ";
            // }
            // cout << '\n';
            // for (int i = 0; i < 4 * n; i++)
            // {
            //     cout << trees[2][i].value << " ";
            // }
            // cout << "\n";
        }
    }

    return 0;
}