// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 200005;
int n;
int nums[MAXN];
vector<bool> occupied(MAXN, false);

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n;
        occupied.assign(MAXN, false);
        bool ans = true;
        for (int i = 0; i < n; i++)
        {
            cin >> nums[i];
            nums[i] = nums[i] % n;
            if (nums[i] < 0) {
                nums[i] += n;
            }
            if (!occupied[(i + nums[i] % n) % n])
            {
                occupied[(i + nums[i] % n) % n] = true;
            }
            else
            {
                ans = false;
            }
        }
        // if (ans)
        // {
        //     for (int i = 0; i < n; i++)
        //     {
        //         if (!occupied[i])
        //         {
        //             ans = false;
        //         }
        //     }
        // }
        if (ans)
        {
            cout << "YES\n";
        }
        else
        {
            cout << "NO\n";
        }
    }

    return 0;
}