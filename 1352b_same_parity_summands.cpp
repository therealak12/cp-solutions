// #include <bits/stdc++.h>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <queue>
#include <set>
#include <unordered_set>
#include <math.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c)) - (c).begin())

const int MAXN = 1000000005;
int n, k;

int main()
{
    FASTER;
    int t;
    cin >> t;
    while (t--)
    {
        cin >> n >> k;
        if (n % 2 == 1 && k % 2 == 0 || k > n)
        {
            cout << "NO\n";
            continue;
        }
        if (n % 2 == 0)
        {
            if (k % 2 == 1 && k * 2 > n)
            {
                cout << "NO\n";
                continue;
            }
            cout << "YES\n";
            if (k % 2 == 1)
            {
                for (; k > 1; --k)
                {
                    cout << "2 ";
                    n -= 2;
                }
            }
            else
            {
                if (k * 2 <= n)
                {
                    for (; k > 1; --k)
                    {
                        cout << "2 ";
                        n -= 2;
                    }
                }
                else
                {
                    for (; k > 1; --k)
                    {
                        cout << "1 ";
                        n -= 1;
                    }
                }
            }
            if (n != 0)
                cout << n;
        }
        else
        {
            cout << "YES\n";
            for (; k > 1; k--)
            {
                cout << "1 ";
                n -= 1;
            }
            if (n != 0)
                cout << n;
        }
        cout << '\n';
    }

    return 0;
}