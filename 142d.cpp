#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)

int n, m, k;
char cells[100][100];
const char dash = '-';
const char second[7] = "Second";
const char first[6] = "First";
const char draw[5] = "Draw";

void scan_cells()
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            cin >> cells[i][j];
        }
    }
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cin >> n >> m >> k;
    scan_cells();
    vector<vector<int>> pile_bits;
    bool second_can_loose, first_can_loose;
    first_can_loose = second_can_loose = true;

    if (m == 1)
    {
        cout << second << endl;
        return 0;
    }
    if (m == 2)
    {
        for (int i = 0; i < n; ++i)
        {
            if (cells[i][0] == dash)
            {
                if (cells[i][1] == 'G')
                {
                    first_can_loose = false;
                }
                else if (cells[i][1] == 'R')
                {
                    second_can_loose = false;
                }
            }
            if (cells[i][1] == dash)
            {
                if (cells[i][0] == 'G')
                {
                    first_can_loose = false;
                }
                else if (cells[i][0] == 'R')
                {
                    second_can_loose = false;
                }
            }
        }
        if (!first_can_loose && !second_can_loose)
        {
            cout << draw << endl;
        }
        else if (!first_can_loose)
        {
            cout << first << endl;
        }
        else
        {
            cout << second << endl;
        }
        return 0;
    }

    for (int i = 0; i < n; ++i)
    {
        char first, second;
        int first_index;
        first = second = dash;
        for (int j = 0; j < m; ++j)
        {
            if (cells[i][j] != dash)
            {
                if (first == dash)
                {
                    first = cells[i][j];
                    first_index = j;
                }
                else
                {
                    second = cells[i][j];
                    if (second == first)
                    {
                        if (first == 'G')
                        {
                            first_can_loose = false;
                        }
                        else
                        {
                            second_can_loose = false;
                        }
                    }
                    else
                    {
                        int dist = j - first_index - 1;

                        vector<int> binary_repr;
                        for (int i = 0; i < 8; ++i)
                        {
                            binary_repr.push_back(dist % 2);
                            dist /= 2;
                        }
                        pile_bits.push_back(binary_repr);
                    }
                    continue;
                }
            }
        }
        if (second == dash)
        {
            if (first == 'G')
            {
                first_can_loose = false;
            }
            else if (first == 'R')
            {
                second_can_loose = false;
            }
        }
    }

    int bit_sums[8];
    for (int i = 0; i < 8; ++i)
    {
        bit_sums[i] = 0;
    }
    for (auto br : pile_bits)
    {
        for (int i = 0; i < 8; ++i)
        {
            bit_sums[i] += br[i];
        }
    }

    for (int i = 0; i < 8; ++i)
    {
        if ((bit_sums[i] % (k + 1)) != 0)
        {
            if (second_can_loose)
            {
                cout << first << endl;
                return 0;
            }
        }
    }

    if (first_can_loose)
    {
        cout << second << endl;
    }
    else if (second_can_loose)
    {
        cout << first << endl;
    }
    else
    {
        cout << draw << endl;
    }

    return 0;
}