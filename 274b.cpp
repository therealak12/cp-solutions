#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define pll pair<ll, ll>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n;
vvi tree;
vector<ll> values;
vector<bool> vis;

pair<ll, ll> dfs(int source)
{
    vis[source] = true;
    ll ch_dec = 0;
    ll ch_inc = 0;
    for (int child : tree[source])
    {
        if (vis[child] == false)
        {
            pll chs = dfs(child);
            ch_inc = max(ch_inc, chs.first);
            ch_dec = max(ch_dec, chs.second);
        }
    }
    ll value = values[source] + ch_inc - ch_dec;
    if (value > 0)
    {
        ch_dec += value;
    }
    else
    {
        value = abs(value);
        ch_inc += value;
    }
    return mp(ch_inc, ch_dec);
}

int main()
{
    FASTER;
    cin >> n;
    tree.resize(n);
    values.resize(n);
    vis.resize(n);
    for (int i = 0; i < n - 1; i++)
    {
        int u, v;
        cin >> u >> v;
        u -= 1;
        v -= 1;
        tree[u].push_back(v);
        tree[v].push_back(u);
    }
    for (int i = 0; i < n; i++)
    {
        cin >> values[i];
    }

    pll ans = dfs(0);
    cout << ans.first + ans.second << "\n";

    return 0;
}
