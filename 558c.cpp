#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);
#define MAXN 1e5 + 10

int n;
vi nums;
vi cost(MAXN, 0);
vi visl(MAXN, 0);
vi rep(MAXN, 0);

int main()
{
    FASTER;
    cin >> n;
    nums.resize(n);
    queue<pii> ndq;
    for (int i = 0; i < n; i++)
    {
        cin >> nums[i];
        ndq.push(mp(nums[i], 0));
        while (!ndq.empty())
        {
            int node = ndq.front().first;
            int di = ndq.front().second;
            ndq.pop();
            if (node >= MAXN || visl[node] == i + 1)
            {
                continue;
            }
            visl[node] = i + 1;
            rep[node] += 1;
            cost[node] += di;
            ndq.push(mp(node * 2, di + 1));
            ndq.push(mp(node / 2, di + 1));
        }
    }

    int ans = INT32_MAX;
    for (int i = 0; i <= MAXN; i++)
    {
        if (rep[i] == n)
        {
            if (ans > cost[i])
            {
                ans = cost[i];
            }
        }
    }

    cout << ans << "\n";

    return 0;
}