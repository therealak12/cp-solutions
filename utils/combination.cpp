#include <bits/stdc++.h>
using namespace std;
using ll = long long;

const int MAXN = 1e5 + 5;
const int MOD = 1e9 + 7;

int inv[MAXN];
int fact[MAXN];

void init()
{
    fact[0] = 1;
    for (int i = 1; i <= MAXN; i++)
    {
        fact[i] = (ll)i * fact[i - 1] % MOD;
    }
    inv[1] = 1;
    for (int i = 2; i <= MAXN; ++i)
    {
        inv[i] = MOD - (ll)(MOD / i) * inv[MOD % i] % MOD;
    }
    inv[0] = 1;
    for (int i = 1; i <= MAXN; i++)
    {
        inv[i] = (ll)inv[i - 1] * inv[i] % MOD;
    }
}

int combination(int n, int k)
{
    if (n < k || k < 0 || n < 0)
    {
        return 0;
    }
    return (ll)fact[n] * inv[k] % MOD * inv[n - k] % MOD;
}

int main()
{
    init();
    int n, m;
    cin >> n >> m;
    cout << combination(n, m) << endl;
    return 0;
}