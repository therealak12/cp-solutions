#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER ios_base::sync_with_stdio(0);

int n;
vi heights;

int main()
{
    FASTER;
    cin >> n;
    heights.resize(n);
    for (int i = 0; i < n; i++)
    {
        cin >> heights[i];
    }

    // ll ans = 0;
    // ll heights_sum = 0;
    // for (int i = 0; i < n; i++)
    // {
    //     heights_sum += heights[i];
    //     ans += heights[i] / 2;
    //     if (i > 0 && heights[i] % 2 == 1 && heights[i - 1] % 2 == 1)
    //     {
    //         ans += 1;
    //         heights[i] -= 1;
    //     }
    // }

    // if (ans > (heights_sum / 2))
    // {
    //     ans = heights_sum - ans;
    // }
    // cout << ans << "\n";

    // A failing example of commented solution is:
    // 4
    // 5 4 3 3

    ll ans = 0;
    ll heights_sum = 0;
    for (int i = 0; i < n; i++)
    {
        heights_sum += heights[i];
        if (heights[i] % 2 == 0)
        {
            ans += heights[i] / 2;
        }
        else
        {
            ans += (heights[i] / 2) + i % 2;
        }
    }

    if (ans > (heights_sum / 2))
    {
        ans = heights_sum - ans;
    }
    cout << ans << "\n";

    return 0;
}