#include <bits/stdc++.h>

using namespace std;
using ll = long long;
#define mp(a, b) make_pair(a, b)
#define all(v) (v).begin(), (v).end()
#define vi vector<int>
#define vvi vector<vi>
#define pii pair<int, int>
#define vii vector<pii>
#define MOD 1000000007
#define eps 1e-8
#define FASTER                    \
    ios_base::sync_with_stdio(0); \
    cin.tie(0);                   \
    cout.tie(0)

const int MAXN = 1001;
int n;
vector<vector<int>> table;
int min_factors[MAXN][MAXN][2];
char choices[MAXN][MAXN][2];

pii count_factors(int number)
{
    int twos = 0;
    int fives = 0;
    if (number == 0)
    {
        return mp(1, 1);
    }
    while (number % 10 == 0)
    {
        number /= 10;
        twos += 1;
        fives += 1;
    }
    while (number % 2 == 0)
    {
        number /= 2;
        twos += 1;
    }
    while (number % 5 == 0)
    {
        number /= 5;
        fives += 1;
    }
    return mp(twos, fives);
}

void print_answer(int which_factor)
{
    vector<char> ans;

    int x = n - 1;
    int y = n - 1;
    while (x > 0 || y > 0)
    {
        ans.push_back(choices[x][y][which_factor]);
        if (choices[x][y][which_factor] == 'D')
        {
            x -= 1;
        }
        else
        {
            y -= 1;
        }
    }
    for (int i = ans.size() - 1; i >= 0; i--)
    {
        cout << ans[i];
    }
    cout << endl;
}

int main()
{
    FASTER;
    cin >> n;
    table.resize(n);
    int zero_pos[2] = {-1, -1};
    for (int i = 0; i < n; i++)
    {
        table[i].resize(n);
        for (int j = 0; j < n; j++)
        {
            cin >> table[i][j];
            if (table[i][j] == 0)
            {
                zero_pos[0] = i;
                zero_pos[1] = j;
            }
        }
    }

    pii factors = count_factors(table[0][0]);
    min_factors[0][0][0] = factors.first;
    min_factors[0][0][1] = factors.second;
    for (int i = 1; i < n; i++)
    {
        factors = count_factors(table[i][0]);
        for (int j = 0; j < 2; j++)
        {
            min_factors[i][0][j] = min_factors[i - 1][0][j] + (j == 0 ? factors.first : factors.second);
            choices[i][0][j] = 'D';
        }

        factors = count_factors(table[0][i]);
        for (int j = 0; j < 2; j++)
        {
            min_factors[0][i][j] = min_factors[0][i - 1][j] + (j == 0 ? factors.first : factors.second);
            choices[0][i][j] = 'R';
        }
    }

    for (int i = 1; i < n; i++)
    {
        for (int j = 1; j < n; j++)
        {
            factors = count_factors(table[i][j]);
            for (int k = 0; k < 2; k++)
            {
                if (min_factors[i - 1][j][k] < min_factors[i][j - 1][k])
                {
                    min_factors[i][j][k] = min_factors[i - 1][j][k] + (k == 0 ? factors.first : factors.second);
                    choices[i][j][k] = 'D';
                }
                else
                {
                    min_factors[i][j][k] = min_factors[i][j - 1][k] + (k == 0 ? factors.first : factors.second);
                    choices[i][j][k] = 'R';
                }
            }
        }
    }

    int last = n - 1;
    if (min_factors[last][last][0] != 0 && min_factors[last][last][1] != 0 && zero_pos[0] != -1)
    {
        cout << "1" << endl;
        for (int i = 0; i < zero_pos[0]; i++)
        {
            cout << 'D';
        }
        for (int i = 0; i < zero_pos[1]; i++)
        {
            cout << 'R';
        }
        for (int i = n - zero_pos[0] - 2; i >= 0; i--)
        {
            cout << 'D';
        }
        for (int i = n - zero_pos[1] - 2; i >= 0; i--)
        {
            cout << 'R';
        }

        cout << endl;
    }
    else
    {
        if (min_factors[last][last][0] < min_factors[last][last][1])
        {
            cout << min_factors[last][last][0] << endl;
            print_answer(0);
        }
        else
        {
            cout << min_factors[last][last][1] << endl;
            print_answer(1);
        }
    }
    // for (int i = 0; i < n; i++)
    // {
    //     for (int j = 0; j < n; j++)
    //     {
    //         cout << min_factors[i][j][1] << " ";
    //     }
    //     cout << endl;
    // }
    return 0;
}